#!/bin/bash

echo "Generating makefiles..."

cmake -B _build_x64_debug -DCMAKE_BUILD_TYPE=Debug -DBOOST_ROOT=~/boost_1_67_0/ -DCMAKE_INSTALL_PREFIX:PATH=../dist/linux_x64/Debug

echo "Building..."
cd _build_x64_debug
make VERBOSE=0 && make install

cd ..

# release
cmake -B _build_x64_release -DCMAKE_BUILD_TYPE=Release -DBOOST_ROOT=~/boost_1_67_0/ -DOPTION_USE_LOGGING=OFF -DCMAKE_INSTALL_PREFIX:PATH=../dist/linux_x64/Release

echo "Building..."
cd _build_x64_release
make VERBOSE=0 && make install

cd ..

# develop = release with logs and symbols
cmake -B _build_x64_develop -DCMAKE_BUILD_TYPE=RelWithDebInfo -DBOOST_ROOT=~/boost_1_67_0/ -DOPTION_USE_LOGGING=ON -DCMAKE_INSTALL_PREFIX:PATH=../dist/linux_x64/Develop

echo "Building..."
cd _build_x64_develop
make VERBOSE=0 && make install

cd ..

