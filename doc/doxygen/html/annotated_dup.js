var annotated_dup =
[
    [ "siggen", null, [
      [ "Data", "structsiggen_1_1_data.html", "structsiggen_1_1_data" ],
      [ "File", "classsiggen_1_1_file.html", "classsiggen_1_1_file" ],
      [ "HashBase", "structsiggen_1_1_hash_base.html", "structsiggen_1_1_hash_base" ],
      [ "HashMD5", "structsiggen_1_1_hash_m_d5.html", "structsiggen_1_1_hash_m_d5" ],
      [ "MTQueue", "classsiggen_1_1_m_t_queue.html", "classsiggen_1_1_m_t_queue" ],
      [ "MTSignatures", "classsiggen_1_1_m_t_signatures.html", "classsiggen_1_1_m_t_signatures" ],
      [ "Options", "classsiggen_1_1_options.html", "classsiggen_1_1_options" ],
      [ "ScopedLog", "structsiggen_1_1_scoped_log.html", "structsiggen_1_1_scoped_log" ],
      [ "SigGen", "classsiggen_1_1_sig_gen.html", "classsiggen_1_1_sig_gen" ],
      [ "ThreadManager", "classsiggen_1_1_thread_manager.html", "classsiggen_1_1_thread_manager" ],
      [ "ThreadManagerBase", "classsiggen_1_1_thread_manager_base.html", "classsiggen_1_1_thread_manager_base" ],
      [ "ThreadManagerEager", "classsiggen_1_1_thread_manager_eager.html", "classsiggen_1_1_thread_manager_eager" ],
      [ "ThreadManagerPowerSaver", "classsiggen_1_1_thread_manager_power_saver.html", "classsiggen_1_1_thread_manager_power_saver" ],
      [ "ThreadManagerSingle", "classsiggen_1_1_thread_manager_single.html", "classsiggen_1_1_thread_manager_single" ]
    ] ]
];