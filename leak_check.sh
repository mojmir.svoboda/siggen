#!/bin/bash

valgrind --leak-check=full dist/linux_x64/Develop/bin/SigGen -i test/test1024.bin -o out -d9 -p1 -t2 | gawk -f tool/logColorize.awk
