#define BOOST_TEST_MODULE TestSigGen
#include <boost/test/unit_test.hpp>
#include "SigGen.h"
#include "Options.h"
#include "Logging.h"
#include "md5sum.h"
#include <vector>

bool compareSigGenToReference (std::vector<char> & siggen_buff, std::vector<char> & md5sum_buff, size_t blockSize, std::string const & inputFile)
{
	{
		siggen::Options options;
		options.SetInputFileName(inputFile);
		//options.SetOutputFileName; // no output file
		options.SetBlockSize(blockSize);
		options.SetThreadCount(2);
		options.SetThreadPolicy(siggen::ThreadingPolicy::PowerSaver);
		options.SetLogLevel(3);
		options.SetHashName("md5");

		siggen::SigGen sigGen(options);
		sigGen.ProcessToBuffer(siggen_buff);
	}

	{
		if (FILE * fd = fopen(inputFile.c_str(), "rb"))
		{
			sum_blockwise_to_buff(fd, blockSize, md5sum_buff);
			fclose(fd);
		}
		else
			BOOST_ERROR("cannot open input file");
	}

	return siggen_buff == md5sum_buff;
}

BOOST_AUTO_TEST_CASE( crosscheck_with_ref_impl )
{

	siggen::initLogging(3);

	{
		std::vector<char> siggenBuff;
		std::vector<char> md5sumBuff;
		std::string inputFile("test/test1024.bin");
		size_t const blockSize = 1024 * 1024;

		BOOST_REQUIRE(compareSigGenToReference(siggenBuff, md5sumBuff, blockSize, inputFile));
	}

	{
		std::vector<char> siggenBuff;
		std::vector<char> md5sumBuff;
		std::string inputFile("test/test2048.bin");
		size_t const blockSize = 1024 * 1024;

		BOOST_REQUIRE(compareSigGenToReference(siggenBuff, md5sumBuff, blockSize, inputFile));
	}

	{
		std::vector<char> siggenBuff;
		std::vector<char> md5sumBuff;
		std::string inputFile("test/test0512.bin");
		size_t const blockSize = 1024 * 1024;

		BOOST_REQUIRE(compareSigGenToReference(siggenBuff, md5sumBuff, blockSize, inputFile));
	}
}
