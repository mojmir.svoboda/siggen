#include "ThreadManagerBase.h"
#include "Logging.h"
#include "Data.h"
#include <stdexcept>
#include "ThreadManagerSingle.h"
#include "ThreadManagerPowerSaver.h"
#include "ThreadManagerEager.h"

namespace siggen {

	std::unique_ptr<ThreadManagerBase> ThreadManagerBase::Factory (ThreadingPolicy policy, Data && data)
	{
		switch (policy)
		{
			case SingleThread:
			{
				LOGMSG(debug) << "Creating ThreadManagerSingle";
				std::unique_ptr<ThreadManagerBase> ptr(new ThreadManagerSingle(std::move(data)));
				return ptr;
			}
			case PowerSaver:
			{
				LOGMSG(debug) << "Creating ThreadManagerPowerSaver";
				std::unique_ptr<ThreadManagerBase> ptr(new ThreadManagerPowerSaver(std::move(data)));
				return ptr;
			}
			case Eager:
			{
				LOGMSG(debug) << "Creating ThreadManagerEager";
				std::unique_ptr<ThreadManagerBase> ptr(new ThreadManagerEager(std::move(data)));
				return ptr;
			}
		}

		throw std::runtime_error("cannot create ThreadManager instance");
	}

} // namespace siggen
