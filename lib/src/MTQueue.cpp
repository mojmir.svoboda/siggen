#include "MTQueue.h"
#include "InvalidBlockIndex.h"
#include "Logging.h"

namespace siggen {

	MTQueue::MTQueue (size_t n, size_t blockSize)
	{
		m_Data.resize(n);

		for (auto & data : m_Data)
		{
			std::get<0>(data) = c_InvalidBlockIndex;
			std::get<1>(data).resize(blockSize);
		}
	}

	void MTQueue::Enqueue (size_t blockIndex, std::vector<char> & data)
	{
		LOGSCOPE();
		LOGMSG(trace) << "Enqueue request for block=" << blockIndex << " data=" << (void*)data.data();

		std::unique_lock<std::mutex> lock(m_Mutex);

		if (m_Size == m_Data.capacity())
		{
			LOGMSG(trace) << "Enqueue failed (full), waiting...";
			// wait until there is any data
			m_CondVarWaitForDequeue.wait(lock, [this] { return !IsFull() || m_Terminate; });
		}

		size_t const idx = m_Size++;

		std::get<0>(m_Data[idx]) = blockIndex;
		std::get<1>(m_Data[idx]).swap(data);
		LOGMSG(debug) << "Enqueued idx=" << idx << " block=" << blockIndex << " qdata=" << (void*)std::get<1>(m_Data[idx]).data() << " data=" << (void*)data.data();

		LOGMSG(trace) << "Queue has new element, notifying waiting thread";
		m_CondVarWaitForEnqueue.notify_one();
	}

	void MTQueue::Dequeue (size_t & blockIndex, std::vector<char> & data)
	{
		LOGSCOPE();
		LOGMSG(trace) << "Dequeue request for block=? data=" << (void*)data.data();

		std::unique_lock<std::mutex> lock(m_Mutex);

		if (m_Size == 0)
		{
			LOGMSG(trace) << "Dequeue failed (empty), waiting...";
			// wait until there is any data
			m_CondVarWaitForEnqueue.wait(lock, [this] { return IsWorkReady() || m_Terminate; });

			if (m_Size == 0)
			{
				// woken up, but queue still 0, terminating
				LOGMSG(trace) << "Dequeue woken up, but queue still empty, terminating";
				blockIndex = c_InvalidBlockIndex;
				return;
			}
		}

		size_t idx = --m_Size;

		blockIndex = std::get<0>(m_Data[idx]);
		data.swap(std::get<1>(m_Data[idx]));
		LOGMSG(debug) << "Dequeued idx=" << idx << " block=" << blockIndex << " qdata=" << (void*)std::get<1>(m_Data[idx]).data() << " data=" << (void*)data.data();

		LOGMSG(trace) << "Dequeued and has room for more, notifying waiting threads";
		m_CondVarWaitForDequeue.notify_one();
	}

	void MTQueue::Terminate ()
	{
		LOGSCOPE();

		std::unique_lock<std::mutex> lock(m_Mutex);
		m_Terminate = true;

		LOGMSG(trace) << "Queue terminating, notifying waiting thread";
		m_CondVarWaitForDequeue.notify_all();
		m_CondVarWaitForEnqueue.notify_all();
	}

} // namespace siggen
