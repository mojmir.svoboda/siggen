#include "Options.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>

namespace siggen
{
		ParseConfigResult Options::ParseCommandLine (int argc, char * argv[])
		{
			namespace opts = boost::program_options;

			try
			{
				opts::options_description desc("Allowed options");
				desc.add_options()
					("input,i", opts::value(&m_InputFileName), "input file")
					("output,o", opts::value(&m_OutputFileName), "output file with checksums")
					("block,b", opts::value(&m_BlockSize)->default_value(m_BlockSizeDefault), "block size")
					("sigfun,s", opts::value(&m_HashName)->default_value("md5"), "signature function")
					("policy,p", opts::value(&m_ThreadPolicy)->default_value(m_ThreadPolicy), "threading policy, 0 (single thread), 1 (power saver), 2 (eager)")
					("threads,t", opts::value(&m_ThreadCount), "number of threads to be used, 0 = auto")
					("debug,d", opts::value(&m_LogLevel)->default_value(3), "debug level, 0 (less debug) to 5 (more debug)")
					("help,h", "print help message")
				;

				opts::variables_map vars;
				opts::store(opts::parse_command_line(argc, argv, desc), vars);
				opts::notify(vars);

				if (vars.count("help"))
				{
					std::cout << desc << std::endl;
					return ParseConfigResult::QuitOnHelp;
				}

				if (vars.count("input") == 0)
				{
					throw std::runtime_error(std::string("missing -input argument"));
				}

				if (vars.count("output") == 0)
				{
					// @TODO: create default from --input
					throw std::runtime_error(std::string("missing -output argument"));
				}

				if (m_BlockSize == 0)
				{
					throw std::runtime_error(std::string("block size has zero length"));
				}

				if (m_ThreadPolicy == SingleThread && m_ThreadCount > 0)
				{
					throw std::runtime_error(std::string("conflicting arguments: -p0 and nonzero -t"));
				}
			}
			catch (std::exception const & e)
			{
				std::cerr << "Cannot parse command line, reason: " << e.what() << std::endl;
				throw;
			}
			return ParseConfigResult::OK;
		}

} // namespace siggen

