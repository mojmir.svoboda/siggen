#include "HashMD5.h"
#include "Logging.h"
#include <boost/uuid/detail/md5.hpp>

namespace siggen {

	constexpr size_t getMD5Size ()
	{
		constexpr size_t const size = sizeof(boost::uuids::detail::md5::digest_type); // size of MD5 (in Bytes)
		return size;
	}

	size_t HashMD5::GetSignatureSize () const
	{
		return getMD5Size();
	}

	void HashMD5::CalculateSignature (char const * data, size_t size, char * digest) const
	{
		// calculate md5 signature
		boost::uuids::detail::md5 hash;
		boost::uuids::detail::md5::digest_type md5_digest;
		hash.process_bytes(data, size);
		hash.get_digest(md5_digest);
		char const * const digest_begin = reinterpret_cast<char const *>(md5_digest);

		// store output
		LOGBUFF("Block signature (md5): ", digest_begin, getMD5Size());
		memcpy(digest, digest_begin, getMD5Size());
	}

} // namespace siggen
