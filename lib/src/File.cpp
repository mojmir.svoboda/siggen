#include "File.h"
#include <cstdio>
#include "Logging.h"
#include <stdexcept>

namespace siggen
{
	File::File (std::FILE * file)
		: m_File(file)
	{
		if (!m_File)
			throw std::runtime_error("file not opened");
		LOGMSG(debug) << "Opened file with handle=" << m_File;
	}

	File::~File ()
	{
		if (m_File)
		{
			LOGMSG(debug) << "Closing file with handle=" << m_File;
			fclose(m_File);
			m_File = nullptr;
		}
	}

	size_t File::TryRead (char * buffer, size_t size)
	{
		size_t const sz = std::fread(buffer, sizeof(char), size, m_File);
		LOGMSG(trace) << "Readed chunk from file with handle=" << m_File << " size=" << sz << " Bytes into buffer=" << (void*)buffer;
		return sz;
	}

	size_t File::TryWrite (char const * buffer, size_t size)
	{
		size_t const sz = std::fwrite(buffer, sizeof(char), size, m_File);
		LOGMSG(trace) << "Wrote chunk to file with handle=" << m_File << " size=" << sz << " Bytes from buffer=" << (void*)buffer;
		return sz;
	}

} // namespace siggen

