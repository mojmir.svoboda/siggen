#include "ThreadManagerPowerSaver.h"
#include "Logging.h"
#include "InvalidBlockIndex.h"
#include <exception>
#include <cstring>

namespace siggen {

	ThreadManagerPowerSaver::ThreadManagerPowerSaver (Data && data)
		: ThreadManager(std::move(data)) // @NOTE: data is moved from here
		, m_Queue(m_Data.m_ThreadCount, m_Data.m_BlockSize)
		, m_Signatures(m_Data.m_InputBlockCount, m_Data.m_Hash->GetSignatureSize())
	{
		// buffer for a block
		m_Block.resize(m_Data.m_BlockSize);

		LOGMSG(debug) << "Spawning consumer threads";
		for (uint32_t t = 0; t < m_Data.m_ThreadCount; ++t)
		{
			m_Threads.push_back(std::thread(&ThreadManagerPowerSaver::ConsumerThread, this, t));
		}
	}

	ThreadManagerPowerSaver::~ThreadManagerPowerSaver ()
	{
		LOGMSG(debug) << "Destroying ThreadManagerPowerSaver";
	}

	void ThreadManagerPowerSaver::ConsumerThread (size_t threadIndex)
	{
		LOGSCOPE();

		// thread-local buffer for data
		std::vector<char> threadBlock(m_Data.m_BlockSize);
		// thread-local buffer for result
		std::vector<char> signature(m_Data.m_Hash->GetSignatureSize());

		LOGMSG(debug) << "Consumer Thread_" << threadIndex << " started, data=" << (void*)threadBlock.data();

		while (!m_Terminate.load())
		{
			LOGMSG(trace) << "Thread_" << threadIndex << " entering another loop";

			// get data or wait until there is any data
			size_t blockIndex = c_InvalidBlockIndex;
			m_Queue.Dequeue(blockIndex, threadBlock);

			if (blockIndex == c_InvalidBlockIndex)
			{
				LOGMSG(trace) << "Thread_" << threadIndex << " dequeued -1, terminating";
				break;
			}

			// hash
			LOGMSG(trace) << "Thread_" << threadIndex << " calculating signature for block=" << blockIndex;
			m_Data.m_Hash->CalculateSignature(threadBlock.data(), threadBlock.size(), signature.data());

			// publish result
			m_Signatures.PostResult(blockIndex, signature);
		}

		LOGMSG(debug) << "Consumer Thread_" << threadIndex << " terminated! data=" << (void*)threadBlock.data();
	}

	void ThreadManagerPowerSaver::ProcessToBuffer (std::vector<char> & result)
	{
		LOGSCOPE();

		// read blocks and enqueue
		size_t blockIndex = 0;
		while (size_t const sz = m_Data.m_InputFile->TryRead(m_Block.data(), m_Data.m_BlockSize))
		{
			LOGMSG(trace) << "---------------------------------------------------------------";

			if (sz < m_Data.m_BlockSize)
			{
				LOGMSG(trace) << (1 + blockIndex) << " / " << m_Data.m_InputBlockCount << " Partial block size=" << sz << " padding=" << m_Data.m_BlockSize - sz;
				memset(m_Block.data() + sz, 0, m_Data.m_BlockSize - sz);
			}
			else
			{
				LOGMSG(trace) << (1 + blockIndex) << " / " << m_Data.m_InputBlockCount << " Full block size=" << sz;
			}

			LOGMSG(trace) << "About to enqueue block from file";
			m_Queue.Enqueue(blockIndex, m_Block);

			LOGMSG(trace) << "Enqueued, trying to read another block";
			++blockIndex;
		}

		LOGMSG(trace) << blockIndex << " / " << m_Data.m_InputBlockCount << " last block, reading loop terminated";

		// wait until all threads complete work
		m_Signatures.WaitForComplete();

		// signalize terminate
		LOGMSG(trace) << "Completed, terminating...";
		m_Queue.Terminate();
		m_Terminate.store(true);

		// join threads
		for (uint32_t t = 0; t < m_Data.m_ThreadCount; ++t)
			m_Threads[t].join();

		// copy signatures to result
		result = m_Signatures.GetResult();
	}

} // namespace siggen

