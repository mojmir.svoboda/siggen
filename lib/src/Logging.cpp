#include "Logging.h"
#if ENABLE_LOGGING
#	include <boost/log/core.hpp>
#	include <boost/log/expressions.hpp>
#endif

namespace siggen
{
	void initLogging (int level)
	{
#if ENABLE_LOGGING
		int const reversed_level = level < boost::log::trivial::fatal ? boost::log::trivial::fatal - level : 0;

		boost::log::trivial::severity_level const l = static_cast<boost::log::trivial::severity_level>(reversed_level);
		boost::log::core::get()->set_filter
		(
			boost::log::trivial::severity >= l
		);
#endif
	}

#if ENABLE_LOGGING
	ScopedLog::ScopedLog (char const * file, int line, char const * fn)
		: m_File(file), m_Line(line), m_Func(fn)
	{
		BOOST_LOG_TRIVIAL(trace) << "{ " << m_Func;
	}

	ScopedLog::~ScopedLog ()
	{
		BOOST_LOG_TRIVIAL(trace) << "} " << m_Func;
	}

#else

	NullSink & getNullSink ()
	{
		static NullSink nullSink;
		return nullSink;
	}
#endif

} // namespace siggen

