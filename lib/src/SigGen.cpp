#include "SigGenLib.h"
#include "Logging.h"
#include "ThreadManagerBase.h"
#include "Data.h"
#include <filesystem>
#include <cstdint>
#include <thread>
#include <stdexcept>

namespace siggen
{
	SigGen::SigGen (Options const & options)
	{
		LOGSCOPE();

		// open output file for binary (over)write
		if (options.GetOutputFileName().length())
			m_OutputFile = std::make_unique<File>(fopen(options.GetOutputFileName().c_str(), "w+b"));

		// prepare data for ThreadManager instance
		Data data; // data for thread manager, to be std::move-d from

		// prepare signature (hash) function
		data.m_Hash = HashBase::Factory(options.GetHashName());

		LOGMSG(info) << "Trying to open input file " << options.GetInputFileName();
		std::filesystem::path in(options.GetInputFileName());

		LOGMSG(debug) << "Testing existence of input file";
		if (!std::filesystem::exists(in))
			throw std::runtime_error("input file does not exist");

		LOGMSG(debug) << "Testing regularity of input file";
		if (!std::filesystem::is_regular_file(in))
			throw std::runtime_error("input file is not regular file");

		LOGMSG(debug) << "Testing size of input file";
		size_t const fileSize = std::filesystem::file_size(in);
		LOGMSG(debug) << "Input file size=" << fileSize;
		data.m_InputFileSize = fileSize;
		data.m_BlockSize = options.GetBlockSize();
		LOGMSG(debug) << "Input file block size=" << data.m_BlockSize;
		bool const full = fileSize % data.m_BlockSize == 0;
		data.m_InputBlockCount = fileSize / data.m_BlockSize + (full ? 0 : 1);
		LOGMSG(debug) << "Input file block count=" << data.m_InputBlockCount;

		// open input file for binary read
		data.m_InputFile = std::make_unique<File>(fopen(options.GetInputFileName().c_str(), "rb"));

		// configure threads
		unsigned const hw_count = std::thread::hardware_concurrency();
		data.m_ThreadCount = options.GetThreadCount();
		LOGMSG(debug) << "Thread count from config=" << data.m_ThreadCount << ", detected max cores=" << hw_count;
		if (data.m_ThreadCount == 0)
		{
			data.m_ThreadCount = hw_count;
			LOGMSG(debug) << "Thread count auto-detected: " << data.m_ThreadCount;
		}
		else if (data.m_ThreadCount > hw_count)
		{
			LOGMSG(warning) << "Thread count=" << data.m_ThreadCount <<" bigger than actual core count=" << hw_count;
		}

		// create thread manager according to policy
		m_ThreadManager = std::move(ThreadManagerBase::Factory(options.GetThreadPolicy(), std::move(data)));
	}

	void SigGen::ProcessToFile ()
	{
		LOGSCOPE();
		std::vector<char> result;
		m_ThreadManager->ProcessToBuffer(result);
		WriteOutputFile(result);
	}

	void SigGen::ProcessToBuffer (std::vector<char> & result)
	{
		LOGSCOPE();
		m_ThreadManager->ProcessToBuffer(result);
	}

	void SigGen::WriteOutputFile (std::vector<char> const & signatures)
	{
		LOGSCOPE();

		LOGMSG(info) << "Writing results to output file";

		size_t const result_size = signatures.size();
		size_t const wrote_size = m_OutputFile->TryWrite(signatures.data(), signatures.size());
		LOGMSG(debug) << "Wrote " << wrote_size << " Bytes out of total " << result_size << " Bytes";
		if (wrote_size < result_size)
		{
			throw std::runtime_error("Cannot write signature to output file");
		}
	}

} // namespace siggen

