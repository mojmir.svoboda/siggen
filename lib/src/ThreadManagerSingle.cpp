#include "ThreadManagerSingle.h"
#include "Logging.h"
#include <exception>
#include <cstring>

namespace siggen {

	ThreadManagerSingle::ThreadManagerSingle (Data && data)
		: ThreadManager(std::move(data))
	{ }

	void ThreadManagerSingle::ProcessToBuffer (std::vector<char> & result)
	{
		LOGSCOPE();

		std::vector<char> block(m_Data.m_BlockSize); // buffer for a single block
		result.resize(m_Data.m_InputBlockCount * m_Data.m_Hash->GetSignatureSize());

		// read block after block
		size_t blockIndex = 0;
		while (size_t const sz = m_Data.m_InputFile->TryRead(block.data(), block.size()))
		{
			if (sz < block.size())
			{
				LOGMSG(trace) << (1 + blockIndex) << " / " << m_Data.m_InputBlockCount << " Partial block size=" << sz << " padding=" << m_Data.m_BlockSize - sz;
				memset(block.data() + sz, 0, block.size() - sz);
			}
			else
			{
				LOGMSG(trace) << (1 + blockIndex) << " / " << m_Data.m_InputBlockCount << " Full block size=" << sz;
			}

			// the place to store signature for this block
			char * const result_ptr = result.data() + blockIndex * m_Data.m_Hash->GetSignatureSize();
			// calculate signature
			m_Data.m_Hash->CalculateSignature(block.data(), block.size(), result_ptr);

			++blockIndex;
		}
	}

} // namespace siggen
