#include "ThreadManager.h"
#include "Logging.h"
#include <stdexcept>

namespace siggen
{
	ThreadManager::ThreadManager (Data && data)
		: m_Data(std::move(data))
	{ }

} // namespace siggen

