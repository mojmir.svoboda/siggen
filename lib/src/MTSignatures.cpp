#include "MTSignatures.h"
#include "InvalidBlockIndex.h"
#include "Logging.h"
#include <cstring>

namespace siggen {

	MTSignatures::MTSignatures (size_t blockCount, size_t hashSize)
		: m_BlocksTotal(blockCount)
		, m_HashSize(hashSize)
	{
		m_Signatures.resize(blockCount * hashSize);
	}

	void MTSignatures::PostResult (size_t blockIndex, std::vector<char> const & blockSignature)
	{
		LOGSCOPE();

		std::unique_lock<std::mutex> lock(m_Mutex);

		// memcpy data
		size_t const signatureOffset = blockIndex * m_HashSize;
		memcpy(m_Signatures.data() + signatureOffset, blockSignature.data(), m_HashSize);

		++m_BlocksWritten;

		// if that's all, wake up waiting main thread
		if (IsComplete())
			m_CondVarWaitForComplete.notify_all();
	}

	void MTSignatures::WaitForComplete ()
	{
		LOGSCOPE();

		std::unique_lock<std::mutex> lock(m_Mutex);

		if (!IsComplete())
		{
			LOGMSG(trace) << "Waiting for all threads to deliver signatures...";
			m_CondVarWaitForComplete.wait(lock, [this] { return IsComplete(); });
		}

		LOGMSG(debug) << "WaitForComplete - all threads delivered signatures";
	}

} // namespace siggen
