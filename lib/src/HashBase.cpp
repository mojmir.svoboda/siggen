#include "HashBase.h"
#include "HashMD5.h"
#include "Logging.h"
#include <exception>
#include <stdexcept>
#include <memory>
//#include "HashSHA1.h"

namespace siggen {

	std::unique_ptr<HashBase> HashBase::Factory (std::string const & name)
	{
		if (name == "md5")
		{
			LOGMSG(debug) << "Creating signature function=" << name;
			return std::make_unique<HashMD5>();
		}

		LOGMSG(error) << "Unknown signature function=" << name;
		throw std::runtime_error("Unknown signature function");
	}

} // namespace siggen
