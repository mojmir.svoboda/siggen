#pragma once
#include <limits>

namespace siggen {

	static size_t const c_InvalidBlockIndex = std::numeric_limits<size_t>::max(); ///< value of an invalid index

} // namespace siggen

