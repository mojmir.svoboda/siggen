#pragma once
#include <memory>
#include <string>

namespace siggen {

	/**@class	HashBase
	 * @brief	base for cryptographic functions
	 **/
	struct HashBase
	{
		virtual ~HashBase () { }

		/**@fn			GetSignatureSize
		 * @brief		returns size of the signature
		 * @return		size of the signature
		 **/
		virtual size_t GetSignatureSize () const = 0;

		/**@fn			CalculateSignature
		 * @brief		calculates signature of the block of data
		 *
		 * @param[in]	data			buffer with data for calculation of signature
		 * @param[in]	size			size of the data
		 * @param[out]	digest			resulting signature
		 *
		 **/
		virtual void CalculateSignature (char const * data, size_t size, char * digest) const = 0;

		/**@fn			Factory
		 * @brief		creates hash according to specified name
		 *
		 * @param[in]	name			specification of the type of hash to be created
		 * @return		created hash class or throws an exception
		 * @throws		std::runtime_error
		 **/
		static std::unique_ptr<HashBase> Factory (std::string const & name);
	};

} // namespace siggen

