#pragma once
#include "ThreadManager.h"

namespace siggen {

	/**@class		ThreadManagerSingle
	 * @brief		single-thread signature generator
	 **/
	class ThreadManagerSingle : public ThreadManager
	{
	public:
		ThreadManagerSingle (Data && data);

		/**@fn		ProcessToBuffer
		 * @brief	calculates block-wise signature of input file and writes result to buffer
		 *
		 * This is a single threaded operation.
		 **/
		virtual void ProcessToBuffer (std::vector<char> & result) override;
	};

} // namespace siggen

