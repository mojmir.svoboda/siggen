#pragma once
#include <string>
#include "ThreadingPolicy.h"

namespace siggen
{
	/**@enum		ParseConfigResult
	 * @brief		result of Options::ParseCommandLine operation
	 **/
	enum class ParseConfigResult : int
	{
		  OK				///< success
		, QuitOnHelp		///< success, help invoked
	};


	/**@class		Options
	 * @brief		configuration for generation of signatures file
	 **/
	class Options
	{
		std::string m_InputFileName;						///< input file name to be processed
		std::string m_OutputFileName;						///< output file with calculated signatures
		size_t m_BlockSizeDefault { 1024 * 1024 };			///< default block size
		size_t m_BlockSize { 1024 * 1024 };					///< size of the block the input will be splitted to
		uint32_t m_ThreadCount { 0 };						///< thread count to use (0 == auto)
		uint32_t m_ThreadPolicy { PowerSaver };				///< thread policy to use
		uint32_t m_LogLevel { 0 };							///< output level verbosity (0 low, 5 high)
		std::string m_HashName;								///< hash to be used

	public:
		/// return user specified input file name
		std::string const & GetInputFileName () const { return m_InputFileName; }
		/// return user specified output file name
		std::string const & GetOutputFileName () const { return m_OutputFileName; }
		/// return user specified block size
		size_t GetBlockSize () const { return m_BlockSize; }
		/// return user specified number of threads (0 == auto)
		uint32_t GetThreadCount () const { return m_ThreadCount; }
		/// return user specified threading policy
		ThreadingPolicy GetThreadPolicy () const { return static_cast<ThreadingPolicy>(m_ThreadPolicy); }
		/// return user specified verbosity
		uint32_t GetLogLevel () const { return m_LogLevel; }
		/// return user specified hash function name
		std::string const & GetHashName () const { return m_HashName; }

		/// set user specified input file name
		void SetInputFileName (std::string const & name) { m_InputFileName = name; }
		/// set user specified output file name
		void GetOutputFileName (std::string const & name) { m_OutputFileName = name; }
		/// set user specified block size
		void SetBlockSize (size_t size) { m_BlockSize = size; }
		/// set user specified number of threads (0 == auto)
		void SetThreadCount (uint32_t count) { m_ThreadCount = count; }
		/// set user specified threading policy
		void SetThreadPolicy (ThreadingPolicy policy) { m_ThreadPolicy = policy; }
		/// set user specified verbosity
		void SetLogLevel (uint32_t level) { m_LogLevel = level; }
		/// set user specified hash function name
		void SetHashName (std::string const & name) { m_HashName = name; }

		/**@fn		ParseCommandLine
		 * @brief	reads options from command line
		 * @param[in]	argc						number of command line arguments
		 * @param[in]	argv						array of command line arguments
		 * @return		ParseConfigResult::OK		on succes
		 * @throws		std::exception				if error encountered
		 **/
		ParseConfigResult ParseCommandLine (int argc, char * argv[]);
	};

} // namespace siggen

