#pragma once
#include <memory>
#include <vector>
#include "ThreadManagerBase.h"
#include "File.h"

namespace siggen
{
	class Options;

	/**@class	SigGen
	 * @brief	generates signature from input file and writes result to output file
	 **/
	class SigGen
	{
		std::unique_ptr<File> m_OutputFile;					///< output file with signatures
		std::unique_ptr<ThreadManagerBase> m_ThreadManager;	///< thread manager

	public:

		/**@fn		SigGen::SigGen
		 * @brief	generates signature from input file and writes result to output file
		 *
		 * Prepares input, output and threading for actual processing by thread manager.
		 *
		 * @throws	std::runtime_error
		 **/
		SigGen (Options const & options);

		/**@fn		ProcessToFile
		 * @brief	reads input file block after block and generate signature for each one
		 *
		 * Writes result to file specified by options
		 **/
		void ProcessToFile ();

		/**@fn		ProcessToBuffer
		 * @brief	reads input file block after block and generate signature for each one
		 *
		 * Writes result to supplied buffer
		 **/
		void ProcessToBuffer (std::vector<char> & result);

	protected:

		/**@fn		WriteOutputFile
		 * @brief	writes signature from memory to file
		 * @throws  std::runtime_error
		 **/
		void WriteOutputFile (std::vector<char> const & signatures);
	};
} // namespace siggen

