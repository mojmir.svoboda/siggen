#pragma once
#include "ThreadManager.h"

namespace siggen {

	// @TODO
	class ThreadManagerEager : public ThreadManager
	{
	public:
		ThreadManagerEager (Data && data) : ThreadManager(std::move(data)) { }

		virtual void ProcessToBuffer (std::vector<char> & result) override;
	};

} // namespace siggen

