#pragma once
#include <cstdio>

namespace siggen
{
	/**@class	File
	 * @brief	basic C file operations encapsulated in class
	 **/
	class File
	{
		std::FILE * m_File { nullptr };

	public:
		/**@fn			File::File
		 * @brief		takes ownership of the opened file descriptor
		 * @throws		std::runtime_error
		 **/
		File (std::FILE * file);

		/**@fn			File::!File
		 * @brief		closes file on destruction
		 **/
		~File ();

		/**@fn			TryRead
		 * @brief		attempts to read block from file
		 *
		 * @param[in]	buffer			buffer for data
		 * @param[in]	size			size of the buffer
		 *
		 * @return		bytes read
		 **/
		size_t TryRead (char * buffer, size_t size);

		/**@fn			TryWrite
		 * @brief		attempts to write block of data to file
		 *
		 * @param[in]	buffer			buffer with data
		 * @param[in]	size			size of the buffer
		 *
		 * @return		bytes written
		 **/
		size_t TryWrite (char const * buffer, size_t size);
	};

} // namespace siggen

