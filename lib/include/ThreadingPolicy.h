#pragma once
#include <cstdint>

namespace siggen
{
	/**@enum		ThreadingPolicy
	 * @brief		the policy to be used for calculation
	 **/
	enum ThreadingPolicy : uint32_t
	{
		  SingleThread
		, PowerSaver
		, Eager
	};

} // namespace siggen

