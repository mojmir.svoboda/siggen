#pragma once
#include "ThreadingPolicy.h"
#include <memory>
#include <vector>

namespace siggen {

	struct Data;

	/**@class	ThreadManagerBase
	 * @brief	base for threading manager
	 **/
	class ThreadManagerBase
	{
	public:
		virtual ~ThreadManagerBase () { }

		/**@fn		ProcessToBuffer
		 * @brief	processes data assigned to thread manager
		 **/
		virtual void ProcessToBuffer (std::vector<char> & result) = 0;

		/**@fn		Factory
		 * @brief	creates threading manager according to ThreadingPolicy
		 *
		 * @param[in]	policy		specification of type of threading manager to be created
		 * @param[in]	data		configuration data to be moved to created threading manager
		 *
		 * @return	threading manager or throws an exception
		 * @throws	std::runtime_error
		 **/
		static std::unique_ptr<ThreadManagerBase> Factory (ThreadingPolicy policy, Data && data);
	};

} // namespace siggen

