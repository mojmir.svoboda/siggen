#pragma once
#include "HashBase.h"

namespace siggen {

	/**@class		HashMD5
	 * @brief		MD5 implementation
	 **/
	struct HashMD5 final : HashBase
	{
		virtual size_t GetSignatureSize () const final;
		virtual void CalculateSignature (char const * data, size_t size, char * digest) const final;
	};

} // namespace siggen
