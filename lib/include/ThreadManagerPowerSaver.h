#pragma once
#include "ThreadManager.h"
#include "MTQueue.h"
#include "MTSignatures.h"
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>

namespace siggen {

	/**@class		ThreadManagerPowerSaver
	 * @brief		default thread manager using mutexes and condition variables
	 **/
	class ThreadManagerPowerSaver : public ThreadManager
	{
		std::atomic<bool> m_Terminate { false };			///< terminate flag for consumer threads
		std::vector<char> m_Block;							///< buffer for a single block
		MTQueue m_Queue;									///< queue for synchronization between producer and consumers
		MTSignatures m_Signatures;							///< class with resulting signatures from consumers
		std::vector<std::thread> m_Threads;					///< spawned consumers

	protected:

		/**@fn		ConsumerThread
		 * @brief	basic loop for spawned consumer threads
		 *
		 * Consumer thread tries to dequeue (block, data) in order to calculate to hash on
		 * dequeued block.
		 * If there is no data, it sleeps on condition variable.
		 * Otherwise it calculates the digest, and posts it to m_Signatures
		 **/
		void ConsumerThread (size_t threadIndex);

	public:
		/**@fn		ThreadManagerPowerSaver::ThreadManagerPowerSaver
		 * @brief	spawns data.m_ThreadCount threads to do the work
		 **/
		ThreadManagerPowerSaver (Data && data);
		virtual ~ThreadManagerPowerSaver ();

		/**@fn		ProcessToBuffer
		 * @brief	reads a block of data from input file, and enqueues the data for further processing by threads
		 *
		 * @param[out]	result		buffer with resulting signatures
		 *
		 * The block of data is swapped with empty block from an idle thread and reading continues.
		 * At the end of the file it waits for all the threads to complete work, then writes resulting
		 * data to the buffer and terminates.
		 **/
		virtual void ProcessToBuffer (std::vector<char> & result) override;
	};

} // namespace siggen

