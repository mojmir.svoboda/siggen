#pragma once

#if !defined ENABLE_LOGGING
	/**	@macro		ENABLE_LOGGING
	 *	@brief		enables/disables logging
	 **/
#	define ENABLE_LOGGING 1
#endif

#if ENABLE_LOGGING
	// logging is enabled, use boost::log
#	include <boost/log/core.hpp>
#	include <boost/log/trivial.hpp>
#	include <boost/log/utility/manipulators/dump.hpp>

#	define TOKEN_CONCAT(x, y) x ## y
#	define TOKEN_CONCAT2(x, y) TOKEN_CONCAT(x, y)
#	define LOG_UNIQUE(name) TOKEN_CONCAT2(name, __LINE__)

	/**	@macro		LOGSCOPE
	 *	@brief		logs "entry to" and "exit from" scope
	 **/
#	define VERBOSE_SCOPES 0
#	if VERBOSE_SCOPES
#		ifdef WIN32
#			define FUNC_SIGNATURE __FUNCSIG__
#		else
#			define FUNC_SIGNATURE __PRETTY_FUNCTION__
#		endif
#	else // VERBOSE_SCOPES
#		define FUNC_SIGNATURE __FUNCTION__
#	endif

	/**	@macro		LOGSCOPE
	 *	@brief		logs "entry to" and "exit from" scope
	 **/
#	define LOGSCOPE()	\
		siggen::ScopedLog LOG_UNIQUE(entry_guard_)(__FILE__, __LINE__, FUNC_SIGNATURE)

	/**	@macro		LOGMSG
	 *	@brief		logs a message
	 **/
#	define LOGMSG BOOST_LOG_TRIVIAL

	/**	@macro		LOGBUFF
	 *	@brief		logs a buffer
	 **/
#	define LOGBUFF(text, data, size)	\
		BOOST_LOG_TRIVIAL(debug) << text << boost::log::dump_elements(data, size)

#else
	// logging is not enabled, use dummies
#	define LOGSCOPE() ((void)0)
#	define LOGMSG(x) siggen::getNullSink()
#	define LOGBUFF(text, data, size) ((void)0)

#endif

namespace siggen
{
	/**@fn			initLogging
	 * @brief		initializes logging
	 *
	 * @param[in]	level		debug level, from 0 to 5
	 **/
	void initLogging (int level = 0);

#if ENABLE_LOGGING
	/**@class		ScopedLog
	 * @brief		RAII class for logging entry on construction and exit on destruction
	 **/
	struct ScopedLog
	{
		char const * m_File;
		int m_Line;
		char const * m_Func;

		ScopedLog (char const * file, int line, char const * fn);
		~ScopedLog ();
	};

#else
	/**@class		NullSink
	 * @brief		/dev/null
	 **/
	struct NullSink
	{
		template<typename T>
		NullSink & operator<< (T &&t) { return *this; }
	};

	NullSink & getNullSink ();
#endif

} // namespace siggen

