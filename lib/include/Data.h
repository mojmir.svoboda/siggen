#pragma once
#include <memory>
#include <vector>
#include "File.h"
#include "HashBase.h"

namespace siggen {

	/**@class	Data
	 * @brief	provides threading manager with necessary data to work with
	 **/
	struct Data
	{
		size_t m_BlockSize { 0 };							///< block size specified from options
		size_t m_InputFileSize { 0 };						///< file size in bytes
		size_t m_InputBlockCount { 0 };						///< block count (number of generated signatures)
		uint32_t m_ThreadCount { 0 };						///< thread count

		std::unique_ptr<File> m_InputFile;					///< input file (binary)
		std::unique_ptr<HashBase> m_Hash;					///< signature function
	};

} // namespace siggen

