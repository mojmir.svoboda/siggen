#pragma once
#include <atomic>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <tuple>

namespace siggen {

	/**@class		MTQueue
	 * @brief		synchronized data passing between producer and consumer threads
	 *
	 * MTQueue uses classic mutex and condition variables for synchronization.
	 * MTQueue is pre-allocated to N elements and does not change it's size.
	 **/
	class MTQueue
	{
		std::mutex m_Mutex;
		std::condition_variable m_CondVarWaitForDequeue;				///< condition to sleep on when queue is full
		std::condition_variable m_CondVarWaitForEnqueue;				///< condition to sleep on when waiting for data
		bool m_Terminate { false };

		size_t m_Size { 0 };											///< actual size of the queue
		std::vector<std::tuple<size_t, std::vector<char>>> m_Data;		///< pre-allocated queue to N elements @see MTQueue::MTQueue

	protected:

		/// queue has at least one item
		bool IsWorkReady () const { return m_Size > 0; }
		/// queue is full
		bool IsFull () const { return m_Size == m_Data.size(); }

	public:

		/**@fn		MTQueue::MTQueue
		 * @brief	creates queue for n blocks with each block of blockSize bytes
		 * @param[in]		n				size of the queue to be pre-allocated for
		 * @param[in]		blockSize		size of each individual block of data
		 *
		 * the queue will be preallocated to N items, each with blockSize bytes of data
		 **/
		MTQueue (size_t n, size_t blockSize);

		/**@fn		Enqueue
		 * @brief	enqueues data or waits
		 *
		 * @param[in]		blockIndex		index of the block of data to be processed
		 * @param[in]		data			data corresponding to the index
		 **/
		void Enqueue (size_t blockIndex, std::vector<char> & data);

		/**@fn		Dequeue
		 * @brief	dequeues data or waits
		 *
		 * @param[out]		blockIndex		index of the block of data to be processed
		 * @param[in]		data			data corresponding to the index
		 **/
		void Dequeue (size_t & blockIndex, std::vector<char> & data);

		/**@fn		Terminate
		 * @brief	sets terminate flag and notify waiting thread(s)
		 **/
		void Terminate ();
	};

} // namespace siggen

