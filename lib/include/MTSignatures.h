#pragma once
#include <mutex>
#include <vector>
#include <condition_variable>

namespace siggen {

	/**@class		MTSignatures
	 * @brief		thread-safe buffer for resulting block-wise signatures
	 **/
	class MTSignatures
	{
		using result_t = std::vector<char>;

		std::mutex m_Mutex;
		std::condition_variable m_CondVarWaitForComplete;	///< condition to sleep on until all threads deliver results
		size_t m_BlocksWritten { 0 };						///< number of blocks already written
		size_t m_BlocksTotal { 0 };							///< total number of blocks
		size_t m_HashSize { 0 };							///< size of an individual signature
		result_t m_Signatures;								///< signatures of each block

	protected:

		/// all of the block signatures arrived
		bool IsComplete () const { return m_BlocksWritten == m_BlocksTotal; }

	public:

		/**@fn		MTSignatures::MTSignatures
		 * @brief	pre-allocates blockCount*hashSize bytes of memory
		 *
		 * @param[in]		blockCount		size of the input file in blocks
		 * @param[in]		hashSize		size of the signature (digest) in bytes
		 **/
		MTSignatures (size_t blockCount, size_t hashSize);

		/**@fn		PostResult
		 * @brief	posting result from another thread
		 *
		 * @param[in]		blockIndex		index of the block of data
		 * @param[in]		blockSignature	signature (digest) of the block corresponding to blockIndex
		 **/
		void PostResult (size_t blockIndex, result_t const & blockSignature);

		/**@fn		WaitForComplete
		 * @brief	waits on the condition until all threads deliver their result
		 **/
		void WaitForComplete ();

		/// get array with result signatures of each block of input
		result_t const & GetResult () const { return m_Signatures; }
	};

} // namespace siggen

