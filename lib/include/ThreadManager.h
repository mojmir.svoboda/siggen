#pragma once
#include "ThreadManagerBase.h"
#include <vector>
#include "Data.h"

namespace siggen {

	/**@class	ThreadManager
	 * @brief	common implementation of a ThreadManager base
	 **/
	class ThreadManager : public ThreadManagerBase
	{
	protected:
		Data m_Data;	///< configuration for a thread manager

	public:
		/**@fn		ThreadManager::ThreadManager
		 * @brief	construct thread manager with data to work on
		 *
		 * warning - the data is moved from
		 **/
		ThreadManager (Data && data);

		virtual void ProcessToBuffer (std::vector<char> & result) = 0;
	};

} // namespace siggen

