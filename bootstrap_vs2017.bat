rem this file is intended for usage from cmd.exe or from explorer (clicking on it)
@echo off

pushd %~dp0

set PATH=C:\Program Files (x86)\CMake\bin;C:\Program Files\CMake\bin;%PATH%

set MSCVER=15
rem NOTE: path to compiler depends on your edition, modify it accordingly, please
rem set MSC_SETUP="C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"
set MSC_SETUP="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvarsall.bat"

set BOOST_ROOT="c:/devel/SigGen/3rd_party/boost"

set INSTDIR=%CD%\vs%MSCVER%.x64\Release
mkdir _build.vs%MSCVER%.64
cd _build.vs%MSCVER%.64
call %MSC_SETUP% x86_amd64
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER% Win64" -DBOOST_ROOT=%BOOST_ROOT% -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build RelWithDebInfo /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..

set INSTDIR=%CD%\vs%MSCVER%.x64\Debug
mkdir _build.vs%MSCVER%.64.Debug
cd _build.vs%MSCVER%.64.Debug
call %MSC_SETUP% x86_amd64
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER% Win64" -DBOOST_ROOT=%BOOST_ROOT% -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build Debug /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..


set INSTDIR=%CD%\vs%MSCVER%.x86\Release
mkdir _build.vs%MSCVER%.32
cd _build.vs%MSCVER%.32
call %MSC_SETUP% x86
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER%" -DBOOST_ROOT=%BOOST_ROOT% -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build RelWithDebInfo /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..

set INSTDIR=%CD%\vs%MSCVER%.x86\Debug
mkdir _build.vs%MSCVER%.32.Debug
cd _build.vs%MSCVER%.32.Debug
call %MSC_SETUP% x86
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER%" -DBOOST_ROOT=%BOOST_ROOT% -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build Debug /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..

goto NOPAUSE

:TERM
pause

:NOPAUSE
popd
