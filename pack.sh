#!/bin/bash

PROGRAM_VERSION=`git describe`

tar czvf siggen_linux_x64_Debug_${PROGRAM_VERSION}.tar.gz -C dist/linux_x64/Debug .
tar czvf siggen_linux_x64_Develop_${PROGRAM_VERSION}.tar.gz -C dist/linux_x64/Develop .
tar czvf siggen_linux_x64_Release_${PROGRAM_VERSION}.tar.gz -C dist/linux_x64/Release .
