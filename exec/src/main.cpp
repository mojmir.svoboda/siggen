/** @file main.cpp
 *  @author Mojmir Svoboda
 **/
#include "SigGenLib.h"
#include <iostream>
#include "Logging.h"

/**@enum		ReturnCodes
 * @brief		possible return codes from executable
 **/
enum class ReturnCodes : int
{
	  OK = 0										///< success
	, QuitOnHelp = 1								///< help was invoked
	, InvalidConfig = 2								///< command line is not valid
	, UnexpectedConfigException = 3					///< unexpected configuration error
	, FailedProcessing = 4							///< processing failed
	, UnexpectedProcessingException = 5				///< processing failed with unexpected error
};

/**@fn			main
 * @retval		0 on success
 * @retval		1 on invoking help
 * @retval		2 on configuration error
 * @retval		3 on unexpected config error
 * @retval		4 on failed processing
 * @retval		5 on unexpected processing error
 **/
int main (int argc, char * argv[])
{
	// parse configuration from command line
	siggen::Options options;
	try
	{
		siggen::ParseConfigResult const result = options.ParseCommandLine(argc, argv);
		if (result == siggen::ParseConfigResult::QuitOnHelp)
			return static_cast<int>(ReturnCodes::QuitOnHelp);
	}
	catch (std::exception const &)
	{
		std::cerr << "Configuration exception caught, terminating." << std::endl;
		return static_cast<int>(ReturnCodes::InvalidConfig);
	}
	catch (...)
	{
		std::cerr << "Unexpected configuration exception caught, terminating." << std::endl;
		return static_cast<int>(ReturnCodes::UnexpectedConfigException);
	}

	siggen::initLogging(options.GetLogLevel());
	LOGSCOPE();
	LOGMSG(info) << "Configuration parsed successfully.";

	// start signature generation
	try
	{
		LOGMSG(info) << "Constructing signature generator...";
		siggen::SigGen sigGen(options);

		LOGMSG(info) << "Processing input file " << options.GetInputFileName();
		sigGen.ProcessToFile();
	}
	catch (std::exception const & e)
	{
		std::cerr << "Exception caught: " << e.what() << std::endl;
		return static_cast<int>(ReturnCodes::FailedProcessing);
	}
	catch (...)
	{
		std::cerr << "Processing exception caught, terminating." << std::endl;
		return static_cast<int>(ReturnCodes::UnexpectedProcessingException);
	}

	LOGMSG(info) << "Terminating with success.";
	return static_cast<int>(ReturnCodes::OK);
}
