rem this file is intended for usage from cmd.exe or from explorer (clicking on it)
@echo off

pushd %~dp0

rem version of msvc for cmake generator
set MSCVER=16
rem NOTE: path to compiler depends on your edition, modify it accordingly, please
set MSC_SETUP="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"
rem set MSC_SETUP="C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvarsall.bat"

rem modify BOOST_ROOT to reflect its location
set BOOST_ROOT="c:/devel/SigGen/3rd_party/boost"

rem path to cmake
set PATH=C:\Program Files (x86)\CMake\bin;C:\Program Files\CMake\bin;%PATH%

rem x64 release
set INSTDIR=%CD%\dist\vs%MSCVER%.x64\Release
mkdir _build.vs%MSCVER%.64
cd _build.vs%MSCVER%.64
call %MSC_SETUP% amd64
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER%" -DBOOST_ROOT=%BOOST_ROOT% -DOPTION_USE_LOGGING=OFF -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build RelWithDebInfo /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..

rem x64 debug
set INSTDIR=%CD%\dist\vs%MSCVER%.x64\Debug
mkdir _build.vs%MSCVER%.64.Debug
cd _build.vs%MSCVER%.64.Debug
call %MSC_SETUP% amd64
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER%" -DBOOST_ROOT=%BOOST_ROOT% -DOPTION_USE_LOGGING=ON -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build Debug /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..

rem x64 release with logging
set INSTDIR=%CD%\dist\vs%MSCVER%.x64\Develop
mkdir _build.vs%MSCVER%.64.Develop
cd _build.vs%MSCVER%.64.Develop
call %MSC_SETUP% amd64
if %errorlevel% neq 0 goto TERM
cmake -G "Visual Studio %MSCVER%" -DBOOST_ROOT=%BOOST_ROOT% -DOPTION_USE_LOGGING=ON -DCMAKE_INSTALL_PREFIX:PATH=%INSTDIR% ..
if %errorlevel% neq 0 goto TERM
devenv SigGen.sln /build RelWithDebInfo /project INSTALL
if %errorlevel% neq 0 goto TERM
cd ..


goto NOPAUSE

:TERM
pause

:NOPAUSE
popd
