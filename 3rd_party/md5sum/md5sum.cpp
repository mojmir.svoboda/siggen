#include "md5sum.h"

void
sum_blockwise(FILE *fd, size_t blocksz)
{
	byte *buf;
	byte digest[16];
	int i, n;
	MD5state *s;

	s = nil;
	n = 0;
	buf = reinterpret_cast<byte *>(malloc(blocksz * sizeof(byte)));
	for(;;)
	{
		i = fread(buf, 1, blocksz, fd);
		if(i <= 0)
			break;

		s = md5(buf, i, 0, s);
		s = md5(buf, 0, digest, s);

		for(i=0;i<16;i++)
			printf("%.2X", digest[i]);
		printf("\n");
	}
	free(buf);
}

int main(int argc, char **argv)
{
	int c;
	FILE *fd;
	int bs = 1024 * 1024;

	argv++; argc--;
	if(argc>0 && strcmp(argv[0],"-d")==0){
		debug++;
		argv++; argc--;
	}

	if(argc>0 && strcmp(argv[0],"-x")==0){
		hex++;
		argv++; argc--;
	}

	for(c = 0; c < argc; c++)
	{
		fd = fopen(argv[c],"r");
		if(fd==NULL){
			fprintf(stderr, "md5sum: can't open %s\n", argv[c]);
			continue;
		}
		//sum(fd, argv[c]);
		sum_blockwise(fd, bs);
		fclose(fd);
	}
	return 0;
}


