#!/bin/bash

valgrind --tool=helgrind  dist/linux_x64/Develop/bin/SigGen -i test/test1024.bin -o out -d3 -p1 -t3 | gawk -f tool/logColorize.awk
valgrind --tool=drd  dist/linux_x64/Develop/bin/SigGen -i test/test1024.bin -o out -d3 -p1 -t3 | gawk -f tool/logColorize.awk
