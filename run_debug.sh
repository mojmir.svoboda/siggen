#!/bin/bash

if [[ "$OSTYPE" == "cygwin" ]]; then
	dist/vs16.x64/Debug/bin/SigGen -i test/2xtest1024.bin -o out -d5 -p1 -t2 | gawk -f tool/logColorize.awk
else
	dist/linux_x64/Debug/bin/SigGen -i test/2xtest1024.bin -o out -d5 -p1 -t2 | gawk -f tool/logColorize.awk
fi
