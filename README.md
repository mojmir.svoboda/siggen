# SigGen

An utility for generating block-wise digest(s) for a binary file.

## Pre-requisities:
Operating system:
Tested with:
* Linux (gcc 8.3.0)
* Windows 10 (msvc 2019)

## Download:

[download](/releases)
 
Windows user may need [redistributable for vs 2019](https://aka.ms/vs/16/release/vc_redist.x64.exe)

## Usage:
```
Program options:
  -i [ --input ] arg            input file
  -o [ --output ] arg           output file with checksums
  -b [ --block ] arg (=1048576) block size
  -s [ --sigfun ] arg (=md5)    signature function
  -p [ --policy ] arg (=1)      threading policy, 0 (single thread), 1 (power 
                                saver), 2 (eager)
  -t [ --threads ] arg          number of threads to be used, 0 = auto
  -d [ --debug ] arg (=3)       debug level, 0 (less debug) to 5 (more debug)
  -h [ --help ]                 print help message
```
## Source documentation:
[doxygen](/doc/doxygen/html/)

## Compilation:

### 1. Preparation
Program uses cmake to generate actual build files:

	[cmake](https://cmake.org/download/)

Tested with 3.15.x

And boost library:

	[boost](https://dl.bintray.com/boostorg/release/1.71.0/source/)
	
Tested with 1.67 on windows and 1.67 on linux
(uuid::md5 in boost 1.71 on linux seems to be broken.)

For simplicity static libraries are used.
Libraries used:
* boost::program_options
* boost::log
* boost::test for unit tests

### 2. Compilation

There are scripts for helping with the build itself, mainly:
* `./bootstrap.sh` on Linux
* `./bootstrap_vs2019.bat` on Windows

You need to specify **BOOST_ROOT**, in both cases - edit the files and modify BOOST_ROOT accordingly.

On windows, you probably need to change **MSC_SETUP** path, as each version/edition has quite different
paths to vcvarsall.bat setup scripts.

Resulting binary should end up in directory `dist`.

### 3. Notes

Program is tested for leaks with valgrind, and for race conditions with Helgrind and Drd.

